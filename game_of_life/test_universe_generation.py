from game_of_life.universe_generation import *
import numpy as np

def test_universe_generation():
    block = [[0, 0, 0, 0], [0, 1, 1, 0], [0, 1, 1, 0], [0, 0, 0, 0]]
    test1 = np.array(block) == np.array(universe_generation(block))  #test on a static pattern
    assert test1.all()
    boat = [[0, 0, 0, 0, 0],
            [0, 1, 1, 0, 0],
            [0, 1, 0, 1, 0],
            [0, 0, 1, 0, 0],
            [0, 0, 0, 0, 0]]
    test2 = np.array(boat) == np.array(universe_generation(boat))    #test on an other static pattern
    assert test2.all()
    blinker1 = [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 1, 1, 1, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]
    blinker2 = [[0, 0, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0]]
    test3 = np.array(blinker2) == np.array(universe_generation(blinker1))  #test on a periodic pattern
    assert test3.all()
    beacon1 = [[0, 0, 0, 0, 0, 0],
               [0, 1, 1, 0, 0, 0],
               [0, 1, 0, 0, 0, 0],
               [0, 0, 0, 0, 1, 0],
               [0, 0, 0, 1, 1, 0],
               [0, 0, 0, 0, 0, 0]]
    beacon2 = [[0, 0, 0, 0, 0, 0],
               [0, 1, 1, 0, 0, 0],
               [0, 1, 1, 0, 0, 0],
               [0, 0, 0, 1, 1, 0],
               [0, 0, 0, 1, 1, 0],
               [0, 0, 0, 0, 0, 0]]
    test4 = np.array(beacon2) == np.array(universe_generation(beacon1))  #test on an other periodic pattern
    assert test4.all()