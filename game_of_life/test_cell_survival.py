from game_of_life.cell_survival import *

def test_survival_2():
    universe1 = [[0, 0, 0], [0, 1, 0], [0, 0, 0]]   #Underpopulation
    assert 0 == survival(universe1, 1, 1)
    universe2 = [[0, 0, 1], [1, 0, 0], [0, 0, 1]]   #Generation
    assert 1 == survival(universe2, 1, 1)
    universe3 = [[0, 0, 1], [0, 1, 0], [0, 0, 1]]   #Stable state
    assert 1 == survival(universe3, 1, 1)
    universe4 = [[0, 1, 0], [1, 1, 0], [1, 0, 1]]   #Overpopulation
    assert 0 == survival(universe4, 1, 1)
    universe5 = [[0, 1, 0], [0, 0, 0], [0, 0, 1]]   #No generation
    assert 0 == survival(universe5, 1, 1)
