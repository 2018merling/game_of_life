seeds = {
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
}

def avalaible_seeds():
    return seeds

def select_seed(size_max):
    allowed_seeds = {}
    for seed_name in seeds:
        length = len(seeds[seed_name])
        width = len(seeds[seed_name][0])
        if length < size_max:
            if width < size_max :
                allowed_seeds[seed_name] = seeds[seed_name]
    print("The seeds: ", tuple(allowed_seeds))
    selected_name = input("Select a name: ")
    if selected_name in allowed_seeds:
        return allowed_seeds[selected_name]
    else:
        print("The input is invalid. You should choose one of the name.")
        return select_seeds(size)