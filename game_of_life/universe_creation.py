
def create_universe(size):
    width, height = size
    universe = []
    for i in range(width):
        line = []
        for j in range(height):
            line.append(0)
        universe.append(line)
    return universe

def add_seed_to_universe(seed, universe, x, y):
    seed_height = len(seed)
    seed_width = len(seed[0])
    universe_height = len(universe)
    universe_width = len(universe[0])
    for i in range(seed_height):
        ii = (x + i) % universe_height
        for j in range(seed_width):
            jj = (y + j) % universe_width
            universe[ii][jj] = seed[i][j]