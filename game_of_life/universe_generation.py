from game_of_life.cell_survival import *

def universe_generation(universe):
    width = len(universe)
    height = len(universe[0])
    new_universe = [[0 for j in range(height)] for i in range(width)]
    for i in range(width):
        for j in range(height):
            new_universe[i][j] = survival(universe, i, j)
    return new_universe