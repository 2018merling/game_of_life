from game_of_life.universe_creation import *
import numpy as np

def test_create_universe():
    assert create_universe((4, 4)) == [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]

def test_add_seed_to_universe():
    seed = [[0, 1, 1], [1, 1, 0], [0, 1, 0]]
    universe = create_universe((6, 6))
    add_seed_to_universe(seed, universe, 1, 1)
    test_equality = (np.array(universe) == np.array([[0, 0, 0, 0, 0, 0],
 [0, 0, 1, 1, 0, 0],
 [0, 1, 1, 0, 0, 0],
 [0 ,0, 1, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0]], dtype=np.uint8))
    assert test_equality.all()
