
def get_cell(universe, i, j):
    size = len(universe)
    i = i % size    #edges are glued together
    j = j % size
    return universe[i][j]

def survival(universe, i, j):
    nearby= []  #this list contains
    nearby.append(get_cell(universe, i - 1, j - 1))
    nearby.append(get_cell(universe, i - 1, j))
    nearby.append(get_cell(universe, i - 1, j + 1))
    nearby.append(get_cell(universe, i, j - 1))
    nearby.append(get_cell(universe, i, j + 1))
    nearby.append(get_cell(universe, i + 1, j - 1))
    nearby.append(get_cell(universe, i + 1, j))
    nearby.append(get_cell(universe, i + 1, j + 1))
    count = len(list(filter(lambda x : x == 1, nearby)))
    if count == 3:
        return 1    #Cell becomes alive
    elif count == 2:
        return universe[i][j]  # Cell does not change
    else:
        return 0    #Cell dies
