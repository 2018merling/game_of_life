from tkinter import *
from functools import partial
from game_of_life.universe_generation import *

def go_forward(universes, time_ptr, window):
    """
    universes: list of generated universe
    t: actual time
    window: where to display
    """
    t = time_ptr[0]
    while t >= len(universes):
        universes.append(universe_generation(universes[len(universes) - 1]))
    current_universe = universes[t]
    display_and_update(current_universe, window)
    time_ptr[0] = t+1
    return

def go_backward(universes, time_ptr, window):
    """
        universes: list of generated universe
        t: actual time
        window: where to display
        """
    t = time_ptr[0]
    if t > 0:
        t -= 1
    if t < 0:
        return
    print(universes, t)
    current_universe = universes[t]
    display_and_update(current_universe, window)
    time_ptr[0] = t
    return

def display_and_update(universe, window):
    heigth, width = len(universe), len(universe[0])
    for i in range(heigth):
        for j in range(width):
            print(window.grid_slaves(row= i, column= j)[0])
            label_cell = window.grid_slaves(row= i, column= j)[0]
            color = 'white'
            if universe[i][j]:
                color = 'black'
            label_cell.configure(bg= color)


def grid_window_creation(universe):
    """universe: the initial configuration"""
    gameoflife_window = Tk()
    # Set buttons to drive the animation
    backward_button = Button(gameoflife_window, text="Backward")
    backward_button.pack(side=LEFT)
    forward_button = Button(gameoflife_window, text="Forward")
    forward_button.pack(side=LEFT)
    # Set the toplevel window displaying the grid
    gameoflife_toplevel = Toplevel(gameoflife_window, height=300, width=300)
    gameoflife_toplevel.title("Game of life")
    gameoflife_toplevel.grid()
    heigth = len(universe)
    width = len(universe[0])
    gameoflife_toplevel.rowconfigure(heigth)
    gameoflife_toplevel.columnconfigure(width)
    for i in range(heigth):
        for j in range(width):
            Label(gameoflife_toplevel, bg='white', relief=RIDGE, width=5).grid(row=i, column=j)
    display_and_update(universe, gameoflife_toplevel)
    # Configure buttons
    universes = [universe]
    time_ptr = [0]
    backward_button.config(command=partial(go_backward, universes, time_ptr, gameoflife_toplevel))
    forward_button.config(command= partial(go_forward, universes, time_ptr, gameoflife_toplevel))
    # Launch
    gameoflife_window.mainloop()


grid_window_creation([[0, 1, 0, 0], [1, 1, 0, 0], [0, 1, 0, 1], [0, 1, 1, 0]])