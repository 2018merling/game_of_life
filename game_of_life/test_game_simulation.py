from game_of_life.game_simulation import *
import numpy as np

def test_game_life_simulate():
    block = [[0, 0, 0, 0], [0, 1, 1, 0], [0, 1, 1, 0], [0, 0, 0, 0]]    # test on a static pattern
    test1 = np.array([block]) == np.array(game_life_simulate(block, 1))
    assert test1.all()
    test2 = np.array([block, block]) == np.array(game_life_simulate(block, 2))
    assert test2.all()
    beacon1 = [[0, 0, 0, 0, 0, 0],
               [0, 1, 1, 0, 0, 0],
               [0, 1, 0, 0, 0, 0],
               [0, 0, 0, 0, 1, 0],
               [0, 0, 0, 1, 1, 0],
               [0, 0, 0, 0, 0, 0]]
    beacon2 = [[0, 0, 0, 0, 0, 0],
               [0, 1, 1, 0, 0, 0],
               [0, 1, 1, 0, 0, 0],
               [0, 0, 0, 1, 1, 0],
               [0, 0, 0, 1, 1, 0],
               [0, 0, 0, 0, 0, 0]]
    test3 = np.array([beacon1, beacon2, beacon1]) == np.array(game_life_simulate(beacon1, 3))
    assert test3.all()    #test on 2-periodic pattern