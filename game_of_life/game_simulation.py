from game_of_life.universe_generation import *

def game_life_simulate(universe, n):
    """This function returns a list of generated universe"""
    list_universes = [universe]
    for t in range(n-1):
        list_universes.append(universe_generation(list_universes[t]))
    return list_universes