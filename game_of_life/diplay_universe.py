import matplotlib.pyplot as plt

def print_universe(universe):
    plt.imshow(universe)
    plt.show()