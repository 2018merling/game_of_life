#Project Title

game-of-life of Gabriel Merlin & Shuwan Wang

#Description

This is a small implementation of Conway's game of life.

##Basic implementation
A basic implementation is contained in the package game_of_life.

##Graphical interface
The implementation with user interface is in the package graphical_interface.
If you launch the module interface_game_of_life_gui, you will obtain a display using only Tkinter.
If you launch the module game_with_pygame, the display of the game is managed by pygame.