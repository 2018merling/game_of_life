from tkinter import *
from random import randint

def construct_seed_location_str(location_str, x, y):
    location_str.set('(' + str(x.get()) + ', ' + str(y.get()) + ')')

def randomly_choose_location(x_var, y_var, height, width):
    x_var.set(randint(0, width-1))
    y_var.set(randint(0, height-1))

def open_location_selection_window(x_var, y_var, height, width, location_str):
    """a window in which the user clicks on the seed's location"""

    def click_cell(evt):
        """action when the user clicks on a cell"""
        i_old = x_var.get()
        j_old = y_var.get()
        i_new = evt.x //8
        j_new = evt.y //8
        try:
            id_old = list_rect[i_old][j_old]
            id_new = list_rect[i_new][j_new]
            canvas.itemconfigure(id_old, fill='white')
            canvas.itemconfigure(id_new, fill= 'black')
            x_var.set(i_new)
            y_var.set(j_new)
            construct_seed_location_str(location_str, x_var, y_var)
        except IndexError:
            return

    def close_window():
        selection_window.destroy()

    selection_window = Toplevel()
    selection_window.title("Choose a location")
    list_rect = []  #this list will contain the tags identifying cells
    # Display the grid corresponding
    canvas = Canvas(selection_window, borderwidth=1, height= 8*height, width= 8*width)
    for i in range(width):
        line = []
        for j in range(height):
            color = 'white'
            if i == x_var.get() and j == y_var.get():
                color = 'black'
            rect = canvas.create_rectangle(8*i, 8*j, 8*(i+1), 8*(j+1), fill= color)
            line.append(rect)
        list_rect.append(line)
    canvas.bind('<Button 1>', click_cell)
    canvas.pack(fill=BOTH, anchor=CENTER, ipadx=4, ipady=2, expand=1)
    # Create a button
    ok_button = Button(selection_window, text="Validate", command=close_window)
    ok_button.pack(anchor='se')