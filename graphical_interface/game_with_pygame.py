import pygame
from graphical_interface.interface_gameoflife_gui import *
from game_of_life.universe_generation import universe_generation

def draw_grid_pygame(surface, universe):
    """draw universe in surface"""
    height = len(universe)
    width = len(universe[0])
    for i in range(height):
        for j in range(width):
            color = (255, 255, 255) #white by default
            if universe[i][j]:
                color = (0, 0, 0)   #black if alive
            pygame.draw.rect(surface, color, (16*j, 16*i, 16*(j+1), 16*(i+1)))

def play(height, width, seed, seed_x, seed_y):
    """launch a game with pygame"""
    # Initiate pygame
    pygame.init()
    screen = pygame.display.set_mode((16*width, 16*height), pygame.RESIZABLE)
    pygame.display.set_caption("Game of life")
    grid_surface = pygame.Surface((16*width, 16*height))

    # Initiate the universe
    universe = create_universe((height, width))
    add_seed_to_universe(seed, universe, seed_x, seed_y)
    draw_grid_pygame(grid_surface, universe)

    # The event loop
    should_continue = True
    while should_continue:
        # Check if quit
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                should_continue = False
                break
        # Generate a new universe
        universe = universe_generation(universe)
        draw_grid_pygame(grid_surface, universe)
        screen.blit(grid_surface, (0, 0))
        pygame.display.flip()

    pygame.quit()

if __name__ == "__main__":
    create_main_window(launchcommand=play, shouldcreatetoplevelwindow=False)