from tkinter import *

def open_seed_selection_window(name_var, seeds):
    """open a window in which the user can select a seed"""
    def close_window():
        selection_window.destroy()

    selection_window = Toplevel()
    selection_window.title("Avalaible seeds")

    # Define radiobutton for each seed
    for seed_name in seeds:
        seed_frame = Frame(selection_window)
        seed_grid = seeds[seed_name]
        grid_height = len(seed_grid)
        grid_width = len(seed_grid[0])
        canvas = Canvas(seed_frame, height=8*grid_height, width=8*grid_width)
        for i in range(grid_height):
            for j in range(grid_width):
                color = 'white'
                if seed_grid[i][j]:
                    color = 'black'
                canvas.create_rectangle(8*j, 8*i, 8*(j+1), 8*(i+1), fill=color)
        seed_button = Radiobutton(seed_frame,
                                  variable= name_var,
                                  value=seed_name,
                                  text= seed_name)
        seed_button.pack(anchor='w')
        canvas.pack(side=RIGHT, anchor='e')
        seed_frame.pack(fill=BOTH, expand=1)

    # Create a button
    ok_button = Button(selection_window, text="Validate", command=close_window)
    ok_button.pack(anchor='se')
