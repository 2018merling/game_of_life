from graphical_interface.seed_selection_gui import open_seed_selection_window
from graphical_interface.seed_location_gui import *
from game_of_life.universe_creation import *
from game_of_life.seeds_management import avalaible_seeds
from graphical_interface.grid_gameoflife import grid

def launch_game(window, height, width, seed, seed_x, seed_y):
    """this function launches game according to its parameters"""

    def close_game():
        game_session.can_run = False
        window.destroy()

    universe = create_universe((height, width))
    add_seed_to_universe(seed, universe, seed_x, seed_y)
    game_session = grid(height, width, root=window)
    window.protocol("WM_DELETE_WINDOW", close_game)
    game_session.set_grid(universe)
    game_session.print_grid()
    game_session.launch()

def create_main_window(launchcommand=launch_game, shouldcreatetoplevelwindow=True):
    """
    Create the main window with which the user interacts.
    The parameters are useful to distinguish when used with a tkinter display or by pygame.
    """
    main_window = Tk()
    main_window.title("game_of_life")

    # Definition of variables
    heigth_str = StringVar()
    heigth_str.set("30")
    heigth_val = IntVar()
    heigth_val.set(30)
    width_str = StringVar()
    width_str.set("30")
    seed_name = StringVar()
    seed_name.set("")
    implantation_selected_method = StringVar()
    implantation_selected_method.set("Random")
    seed_location_x = IntVar()
    seed_location_x.set(0)
    seed_location_y = IntVar()
    seed_location_y.set(0)
    seed_location_str = StringVar()
    seed_location_str.set("(0, 0)")
    seeds = avalaible_seeds()

    # Definition of auxiliary functions

    def validate_dimension_input(new_input):
        """valid an entry iff it corresponds to an integer"""
        try:
            i = int(new_input)
            if i <= 0:
                return False
            # If size change, seed's location can become invalid
            x = seed_location_x.get() % int(width_str.get())
            y = seed_location_y.get() % int(heigth_str.get())
            seed_location_x.set(x)
            seed_location_y.set(y)
            # Update the string showing seed's location
            construct_seed_location_str(seed_location_str, seed_location_x, seed_location_y)
            return True
        except ValueError:
            return False

    def click_select_seed():
        """action to allow user to select a seed"""
        open_seed_selection_window(seed_name, seeds)

    def choose_seed_location():
        """action to allow user to define seed's location"""
        height = int(heigth_str.get())
        width = int(width_str.get())
        if implantation_selected_method.get() == "User-positioned":
            open_location_selection_window(seed_location_x, seed_location_y, height, width, seed_location_str)
        else:
            randomly_choose_location(seed_location_x, seed_location_y, height, width)
            construct_seed_location_str(seed_location_str, seed_location_x, seed_location_y)

    def launch():
        """to launch the game"""
        height = int(heigth_str.get())
        width = int(width_str.get())
        if shouldcreatetoplevelwindow:
            grid_window = Toplevel(main_window)
            launchcommand(grid_window, height, width, seeds[seed_name.get()], seed_location_x.get(), seed_location_y.get())
        else:
            launchcommand(height, width, seeds[seed_name.get()], seed_location_x.get(), seed_location_y.get())

    # Entry for grid's heigth
    validation_command = (main_window.register(validate_dimension_input), '%P')
    heigth_label = Label(main_window, text="Heigth :")
    heigth_entry = Entry(main_window, textvariable=heigth_val, validate= 'all', validatecommand= validation_command)
    heigth_label.grid(row=0, column=0)
    heigth_entry.grid(row=0, column=1)

    # Entry for grid's width
    width_label = Label(main_window, text="Width :")
    width_entry = Entry(main_window, textvariable=width_str, validate='key', validatecommand= validation_command)
    width_label.grid(row=1, column=0)
    width_entry.grid(row=1, column=1)

    # Select seed
    seed_label1 = Label(main_window, text="Selected seed:")
    seed_label2 = Label(main_window, textvariable=seed_name)
    seed_button = Button(main_window, text="Select...", command=click_select_seed)
    seed_label1.grid(row=2, column=0)
    seed_label2.grid(row=2, column=1)
    seed_button.grid(row=2, column=2)

    # Method to implant seed
    implantation_label = Label(main_window, text="Method to implant:")
    implantation_button = Button(main_window, text="Choose location", command= choose_seed_location)
    implantation_menu = OptionMenu(main_window, implantation_selected_method, "Random", "User-positioned")
    implantation_label.grid(row=3, column=0)
    implantation_menu.grid(row=3, column=1)
    implantation_button.grid(row=3, column=2)
    # Implantation's location
    location_label = Label(main_window, text="Location :")
    location_tuple_label = Label(main_window, textvariable=seed_location_str)
    location_label.grid(row=4, column=1)
    location_tuple_label.grid(row=4, column=2)

    # Launch button
    launch_button = Button(main_window, text="Launch", command= launch)
    launch_button.grid(row=5, column=2)
    main_window.mainloop()

if __name__ == '__main__':
    create_main_window()