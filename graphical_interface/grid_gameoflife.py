import time
from tkinter import *
import numpy as np

class cell(object):
    def __init__(self,lives):
        self.lives = lives

    def set(self, alive):
        self.lives=alive

class grid(object):
    gen  = 0
    def __init__(self,height,width,file=None,root=None,):
        if(file is None):
            if root is None:
                self.root=Tk()
                self.root.title("Game of Life")
            else:
                self.root = root
            self.canvas=Canvas(self.root,height =height*4, width=width*4)
            self.canvas.grid(row=0,column=0)
            self.matrix=[[cell(False) for x in range(width)] for y in range(height)]
            self.next_grid = [[cell(False) for x in range(width)] for y in range(height)]
            self.width= width
            self.height=height
            self.can_run = True
        else:
            pass
            #comes later with fileinput

    def print_grid(self):
        for i in range(self.height):
            for j in range(self.width):
                if(self.matrix[i][j].lives):
                    pass
                    self.canvas.create_rectangle(j*4,i*4,j*4+4,i*4 + 4,fill="black")
                elif(not self.matrix[i][j].lives):
                    self.canvas.create_rectangle(j*4,i*4,j*4+5,i*4 + 5,fill="white")#invalid command name ".9727696"?!

    def set_cell(self, height,width,live):
        self.matrix[height][width].set(live)

    def set_grid(self, universe):
        for i in range(self.height):
            for j in range(self.width):
                self.set_cell(i, j, universe[i][j])

    def count_neighbours(self,height,width):
        counter = 0
        for hi in range(height-1,height+2):
             for wi in range(width -1,width+2):
                if(hi < self.height and hi > -1 and wi < self.width and wi > -1):
                   if(self.matrix[hi][wi].lives):
                        counter = counter +1

        if(self.matrix[height][width].lives):
            counter = counter -1
        return counter

    def does_survive(self,i,j):
        neighbours = self.count_neighbours(i,j)
        result =False
        if(self.matrix[i][j].lives):
            if(neighbours == 2 or neighbours == 3):
               result = True
        elif (neighbours == 3):
            result =  True
        return result


    def next_gen(self):
        self.next_grid = [[cell(False) for x in range(self.width)] for y in range(self.height)]
        self.gen = self.gen +1
        for i in range(self.height):
            for j in range(self.width):
                self.next_grid[i][j].set(self.does_survive(i,j))
        self.matrix = self.next_grid

    def launch(self):
        while self.can_run:
            time.sleep(0.1)
            self.print_grid()
            self.next_gen()
            self.canvas.update()


def test():
    cell1=cell(True)
    cell2=cell(False)
    cell2.set(True)
    place=grid(70,70)
    array = np.random.randint(50,60,size=6)
    print(array)
    i = j = 0
    for i in range(len(array)):
        for j in range(len(array)):
            place.set_cell(array[i],array[j],True)
    place.print_grid()
    for i in range(100):
        place.update_grid()
        time.sleep(0.1)
        place.next_gen()
        place.canvas.update()

if __name__ == '__main__':
    test()